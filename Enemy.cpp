#include "Enemy.h"

Enemy::Enemy()
  : killed(false)
{
}

bool Enemy::isKilled()
{
  return killed;
}
