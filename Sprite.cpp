#include "Sprite.h"

Sprite::Sprite(SPRITE_TYPE spriteType, SDL_Surface* img, int x, int y, std::list<SDL_Rect*>& collision)
  : spriteType(spriteType), img(img), collision(collision), dead(false)
{
  pos.x = x;
  pos.y = y;
  pos.w = getImg()->w;
  pos.h = getImg()->h;
}

Sprite::~Sprite()
{
}

SDL_Surface* Sprite::getImg()
{
  return img;
}

void Sprite::refresh()
{
  SDL_Rect const& border = SDL_GetVideoSurface()->clip_rect;
  //Sets dead to true if the sprite leaves the game's borders
  if(getX() <= -img->w || getY() <= -img->h || getX() > border.w || getY() > border.h)
    {
      dead = true;
    }
}

bool Sprite::isDead()
{
  return dead;
}

void Sprite::collide(Sprite* s)
{
  for(auto boxA = this->collision.begin(); boxA != this->collision.end(); ++boxA)
    {
      for(auto boxB = s->collision.begin(); boxB != s->collision.end(); ++boxB)
	{
	  int leftA, leftB, rightA, rightB, topA, topB, bottomA, bottomB;
	  leftA = this->getX() + (*boxA)->x;
	  leftB = s->getX() + (*boxB)->x;
	  rightA = leftA + (*boxA)->w;
	  rightB = leftB + (*boxB)->w;
	  topA = this->getY() + (*boxA)->y;
	  topB = s->getY() + (*boxB)->y;
	  bottomA = topA + (*boxA)->h;
	  bottomB = topB + (*boxB)->h;

	  if(bottomA <= topB ||
	     topA >= bottomB ||
	     rightA <= leftB ||
	     leftA >= rightB)
	    {
	      return;
	    }
	  else
	    {
	      onCollide(s);
	    }
	}
    }
}

SDL_Rect* Sprite::getPos()
{
  return &pos;
}

int Sprite::getW()
{
  return getImg()->w;
}

int Sprite::getH()
{
  return getImg()->h;
}

int Sprite::getX()
{
  return pos.x;
}

int Sprite::getY()
{
  return pos.y;
}

void Sprite::draw()
{
  SDL_Rect posRect; //Copy of pos. Used to prevent SDL_BlitSurface from changing x or y positions when < 0

  posRect.x = pos.x;
  posRect.y = pos.y;
  posRect.h = pos.h;
  posRect.w = pos.w;
  SDL_BlitSurface(getImg(), NULL, SDL_GetVideoSurface(), &posRect);
}

SDL_Surface* Sprite::loadImg(std::string const& file)
{
  SDL_Surface* temp = nullptr;
  //SDL_Surface* temp2 = nullptr;

  temp = IMG_Load(file.c_str());
  // if(temp != nullptr)
  //   {
  //     temp2 = SDL_DisplayFormat(temp);
  //     SDL_FreeSurface(temp);
  //   }
  return temp;
}
