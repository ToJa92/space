#ifndef SHIP
#define SHIP
#include "Sprite.h"
#include "Bullet.h"
#include <string>
#include <list>

/**
 * \brief An abstract class for representing a Ship.
 */
class Ship : public Sprite
{
 public:
  /**
   * \brief Constructs Ship objects
   * 
   * Constructs a Ship object and initializes it.
   */
  Ship(SPRITE_TYPE spriteType, SDL_Surface* img, int x, int y, int HP, std::list<SDL_Rect*>& collision);
  
  ~Ship();

  /**
   * \brief Calls #Bullet::refresh() for the Ship's Bullets.
   * 
   * Deletes and erases dead Bullets, otherwise calls #Bullet::refresh()
   */
  virtual void refresh();

  /**
   * \brief Abstract method for shooting a Bullet.
   */
  virtual void shoot() = 0;

  /**
   * \brief Calls #Sprite::collide() and #Bullet::collide() for the Ship's Bullets
   */
  void collide(Sprite* s);

  /**
   * @return #HP
   */
  int getHP();

  /**
   * \brief Calls #Sprite::draw() and #Bullet::draw() for the Ship's Bullets
   */
  void draw();
  
 protected:
  int HP; //!< The Ship's HP
  std::list<Bullet*> bullets; //!< A list containing the Ship's Bullets
};

#endif
