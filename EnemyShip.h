#ifndef ENEMYSHIP
#define ENEMYSHIP
#include "Ship.h"
#include "Enemy.h"
#include <iostream>

/**
 * \brief An abstract class for representing an EnemyShip.
 */
class EnemyShip : public Ship, public Enemy
{
 public:
  /**
   * \brief Constructs EnemyShip objects
   * 
   * Constructs an EnemyShip object and initializes it.
   */
  EnemyShip(int x, int y, int HP, SDL_Surface* img, std::list<SDL_Rect*>& coll);
  
  ~EnemyShip();

  /**
   * \brief Shoots Bullets and moves the EnemyShip.
   *
   * Calls shoot(), move(), Ship::refresh() and if not dead, calls Sprite::refresh()
   */
  virtual void refresh();

  /**
   * @return #dead
   */
  bool isDead();

  /**
   * \brief Abstract method for movement pattern.
   */
  virtual void move() = 0;

  /**
   * \brief Collision handler for the EnemyShip.
   *
   * Defines the collision behavior for EnemyShips
   */
  void onCollide(Sprite* s);

  /**
   * \brief Shoots a Bullet.
   *
   * Spawns a Bullet in front of the EnemyShip traveling the direction of the Ship.
   */
  virtual void shoot();
 private:
  Uint32 lastShot;//!< Time of the last shot
};
#endif
