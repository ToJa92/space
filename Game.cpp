#include "Game.h"

Game::Game(int w, int h)
  : WIDTH(w), HEIGHT(h), playerScore(0)
{  
  if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) == -1)
    {
      std::cerr << "Couldn't initialize video!" << SDL_GetError() << std::endl;
    }

  screen = SDL_SetVideoMode(WIDTH, HEIGHT, 32, SDL_SWSURFACE | SDL_DOUBLEBUF);
 
  if(screen == NULL)
    {
      std::cerr << "Unable to set mode: " << SDL_GetError() << std::endl;
    }

  if (TTF_Init() == -1) 
  {
    std::cerr << "Unable to init font library" << SDL_GetError() << std::endl;
  }

  menuFont = TTF_OpenFont("04B.ttf", 140);
  pauseFont = TTF_OpenFont("04B.ttf", 70);
  scoreFont = TTF_OpenFont("04B.ttf", 28);

  pauseText = TTF_RenderText_Solid(pauseFont, "PAUSE", SDL_Color {0,0,0,255});
  newGameText = TTF_RenderText_Solid(menuFont, "NEW GAME", SDL_Color {0,0,0,255});
  highScoreText = TTF_RenderText_Solid(menuFont, "HIGHSCORE", SDL_Color {0,0,0,255});
  exitGameText = TTF_RenderText_Solid(menuFont, "EXIT GAME", SDL_Color {0,0,0,255});
  Sint16 menuX = (screen->clip_rect.w / 2) - (newGameText->clip_rect.w / 2);
  Sint16 menuY = screen->clip_rect.h / 4;
  Sint16 pauseX = (screen->clip_rect.w / 2) - (pauseText->clip_rect.w / 2);
  pauseRect = new SDL_Rect {pauseX, 0, 0, 0};
  Sint16 factor = menuY;
  newGameRect = new SDL_Rect {menuX, factor,0,0};
  factor = menuY * 2;
  highScoreRect = new SDL_Rect {menuX, factor,0,0};
  factor = menuY * 3;
  exitGameRect = new SDL_Rect {menuX, factor,0,0};
  loadHighScore();
}

Game::~Game()
{
  PlayerShip::freePlayerShip();
  Meteor::freeMeteor();
  SinShip::freeEnemyShip();
  ZigShip::freeEnemyShip();
  Bullet::freeBullet();
  TTF_CloseFont(menuFont);
  TTF_CloseFont(pauseFont);
  TTF_CloseFont(scoreFont);
  TTF_Quit();
  SDL_FreeSurface(pauseText);
  SDL_FreeSurface(newGameText);
  SDL_FreeSurface(highScoreText);
  SDL_FreeSurface(exitGameText);
  SDL_Quit();
  delete pauseRect;
  delete newGameRect;
  delete highScoreRect;
  delete exitGameRect;
}

void Game::start()
{
  srand(SDL_GetTicks());

  while(showMenu)
    {
      menu();
      
      if(inGame)
	playGame();
    }
}

void Game::playGame()
{
  player = new PlayerShip(350, 50, 10);

  while(inGame)
    {
      time = SDL_GetTicks();
      
      SDL_FillRect(screen, NULL, 0xFFFFFF);

      refresh();
      std::string scoreString = "SCORE: " + std::to_string(playerScore) + " HP: " + std::to_string(player->getHP());
      scoreText = TTF_RenderText_Solid(scoreFont, scoreString.c_str(), SDL_Color {0,0,0,255});
      SDL_BlitSurface(scoreText, NULL, screen, NULL);
      SDL_FreeSurface(scoreText);
      if (player->isDead())
	break;
      
      SDL_Flip(screen);
      if(SDL_GetTicks() - time < FPS)
	SDL_Delay(FPS - (SDL_GetTicks() - time));

      SDL_PumpEvents();
      key = SDL_GetKeyState(NULL);

      if(key[SDLK_w] || key[SDLK_UP]) player->move('u');
      if(key[SDLK_s] || key[SDLK_DOWN]) player->move('d');
      if(key[SDLK_a] || key[SDLK_LEFT]) player->move('l');
      if(key[SDLK_d] || key[SDLK_RIGHT]) player->move('r');
      if(key[SDLK_SPACE]) player->shoot();
      if(key[SDLK_ESCAPE]){ SDL_Delay(75); inGame = false; }
      if(key[SDLK_p]){ SDL_Delay(75); pause(); }
    }

  endGame();
}

void Game::refresh()
{
  if(rand()%10 == 0 && meteorList.size() < (playerScore / 75u) + 1 && meteorList.size() < 6u)
    meteorList.push_back(new Meteor(screen->clip_rect.w, rand()%screen->clip_rect.h));

  if(rand()%10 == 0 && enemyList.size() < (playerScore / 50u) + 1)
    enemyList.push_back(new SinShip(screen->clip_rect.w, rand()%screen->clip_rect.h, 1));

  if(rand()%10 == 0 && enemyList.size() < (playerScore / 50u) + 1)
    enemyList.push_back(new ZigShip(screen->clip_rect.w, rand()%screen->clip_rect.h, 1));
  
  
  for(std::list<Meteor*>::iterator m = meteorList.begin(); m != meteorList.end();)
    {
      if((*m)->isDead())
	{
	  delete *m;
	  m = meteorList.erase(m);
	}
      else 
	{
	  (*m)->refresh();
	  (*m)->draw();
	  (*m)->collide(player);
	  player->collide(*m);
	  ++m;
	}
    }
  
  for(std::list<EnemyShip*>::iterator e = enemyList.begin(); e != enemyList.end();) 
    {
      if ((*e)->isDead())
	{
	  if((*e)->isKilled())
	    addPoints((*e)->getPoints());

	  delete *e;
	  e = enemyList.erase(e);
	}
      else
	{
	  (*e)->refresh();
	  (*e)->draw();
	  for(Meteor* m : meteorList)
	    (*e)->collide(m);
	  
	  (*e)->collide(player);
	  if (!(*e)->isKilled())
	    player->collide(*e);
	  ++e;
	}
    }
  
  player->refresh();
  player->draw();
}

void Game::endGame()
{
  for(auto x : meteorList)
    delete x;

  meteorList = std::list<Meteor*>();
  
  for(auto x : enemyList)
    delete x;

  enemyList = std::list<EnemyShip*>();

  delete player;

  saveHighScore(playerScore);

  playerScore = 0;

  inGame = false;

  highScore();
}
void Game::pause()
{
  SDL_BlitSurface(pauseText, NULL, screen, pauseRect);
  SDL_Flip(screen);
  
  for(;;)
    {
      SDL_PumpEvents();
      key = SDL_GetKeyState(NULL);
      if(key[SDLK_p]){ SDL_Delay(75); return; }
      SDL_Delay(10);
    }
}

void Game::addPoints(int score)
{
  playerScore += score;
}

void Game::menu()
{
  int choice = 1;

  Meteor m(0,0);
  SinShip s1(screen->clip_rect.w, 125, 1);
  SDL_Surface *temp = m.getImg();
  SDL_Rect tempRect = SDL_Rect {0, 0, 0, 0};

  Uint32 keyTime = SDL_GetTicks();

  for(;;)
    {
      tempRect.x = (screen->clip_rect.w/2) - (newGameText->clip_rect.w/2) - m.getW() - 10;
      tempRect.y = (screen->clip_rect.h/4) * choice + (newGameText->clip_rect.h / 2) - (m.getH() / 2) - 5;
      SDL_FillRect(screen, NULL, 0xFFFFFF);
      SDL_BlitSurface(newGameText, NULL, screen, newGameRect);
      SDL_BlitSurface(highScoreText, NULL, screen, highScoreRect);
      SDL_BlitSurface(exitGameText, NULL, screen, exitGameRect);
      SDL_BlitSurface(temp, NULL, screen, &tempRect);
      s1.refresh();
      s1.draw();
      SDL_Flip(screen);
      SDL_PumpEvents();
      key = SDL_GetKeyState(NULL);
      if(SDL_GetTicks() > keyTime + 100)
	{
	  if((key[SDLK_w] || key[SDLK_UP]) && choice > 1){ choice -= 1; }
	  if((key[SDLK_s] || key[SDLK_DOWN]) && choice < 3){ choice += 1; }
	  if(key[SDLK_SPACE] || key[SDLK_RETURN])
	    {
	  
	      switch(choice)
		{
		case 1:
		  inGame = true;
		  return;
		  break;
		case 2:
		  highScore();
		  break;
		case 3:
		  showMenu = false;
		  return;
		  break;
		default:
		  break;
		}
	    }
	  keyTime = SDL_GetTicks();
	}
      SDL_Delay(10);
    }
}

void Game::highScore()
{
  SDL_Surface *scoreSurface;
  SDL_Rect scoreRect {0,0,0,0};
  Sint16 scoreY = screen->clip_rect.h / 11;
  int i = 0;
  SDL_FillRect(screen, NULL, 0xFFFFFF);
  
  for(int score : highscores)
    {
      ++i;
      if(i > 10)
	break;
      scoreSurface = TTF_RenderText_Solid(scoreFont, std::to_string(score).c_str(), SDL_Color {0,0,0,255});
      scoreRect.x = (screen->clip_rect.w/2) - (scoreSurface->clip_rect.w/2);
      scoreRect.y = scoreY * i;
      SDL_BlitSurface(scoreSurface, NULL, screen, &scoreRect);
      SDL_FreeSurface(scoreSurface);
    }

  SDL_Flip(screen);
  
  for(;;)
    {
      SDL_PumpEvents();
      key = SDL_GetKeyState(NULL);
      if(key[SDLK_q] || key[SDLK_ESCAPE]) break;
    }
}

void Game::loadHighScore()
{
  try
    {
      std::ifstream scoreFile("highscore.txt");
      scoreFile.exceptions(std::ios::eofbit | std::ios::failbit | std::ios::badbit);

      int score;

      try
	{
	  while(scoreFile >> score)
	    {
	      highscores.push_back(score);
	    }
	}
      catch(std::exception& e)
	{
	}
  
      scoreFile.close();

    }
  catch(std::exception& e)
    {
      std::ofstream scoreFile("highscore.txt");
      scoreFile.close();
    }
}

void Game::saveHighScore(int score)
{
  std::ofstream scoreFile("highscore.txt", std::ios_base::trunc);
  scoreFile.exceptions(std::ios::eofbit | std::ios::failbit | std::ios::badbit);
  
  highscores.push_front(score);
  highscores.sort(std::greater<int>());

  int i = 0;
  for(int tempScore : highscores)
    {
      if(i > 10)
	break;
      
      scoreFile << tempScore << "\n";
      ++i;
    }
    
  scoreFile.close();
}
