#ifndef SINSHIP
#define SINSHIP
#include "EnemyShip.h"
#include <cmath>

/**
 * \brief A class for representing a SinShip.
 */
class SinShip : public EnemyShip
{
public:
  /**
   * \brief Constructs SinShip objects
   * 
   * Constructs a SinShip object and initializes it.
   */
  SinShip(int x, int y, int HP);
  /**
   * \brief Moves the SinShip according to a pattern
   */
  void move();
  /**
   * \brief Returns the number of #points awarded when killed
   */
  int getPoints();
  /**
   * \brief Static method for freeing the loaded image
   * 
   * Returns the memory allocated for SinShip images.
   */
  static void freeEnemyShip();
private:
  int startY; //!< Start y coordinate
  double doubleY; //!< Y coordinate as a floating point number
  static int points; //!< Number of points
  static SDL_Surface* img; //!< Static image for all SinShips
  static std::list<SDL_Rect*> coll; //!< A list of all SinShips' collision boxes
};
#endif
