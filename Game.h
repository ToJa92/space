#ifndef GAME
#define GAME
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <cstdlib>
#include <ctime>
#include "Sprite.h"
#include "Enemy.h"
#include "Meteor.h"
#include "PlayerShip.h"
#include "EnemyShip.h"
#include "SinShip.h"
#include "ZigShip.h"
#include "Bullet.h"
#include <list>
#include <string>
#include <iostream>
#include <exception>
#include <fstream>

/*! \mainpage SPACE
 *
 * \section intro_sec Installation
 *
 * To compile SPACE, run:
 * 
 * make
 * 
 * To run SPACE, run:
 * 
 * ./SPACE
 * 
 */

/**
 * \brief A class representing the Game
 */
class Game
{
 public:
  /**
   * \brief Constructs a Game object
   * 
   * Initalizes the SDL and SDL_TTF libraries. Opens fonts used in-game.
   * Creates text surfaces and calulates their positions.
   * Loads the high score from file.
   */
  Game(int w, int h);
  
  /**
   * Frees all used surfaces, quits the SDL and SDL_TTF libraries.
   */
  ~Game();
  
  /**
   * Initializes the random number generator. Shows the start menu.
   */
  void start();
  
  /**
   * \brief Contains the main game loop and event handler
   *
   * Refreshes the screen, handles user input, limits the FPS, prints the score and health.
   */
  void playGame();
  
  /**
   * \brief Refreshes the game
   *
   * Spawns enemies, removes dead enemies, refreshes enemies and the player
   */
  void refresh();
  
  /**
   * \brief Resets the game.
   *
   * Deletes all enemies, saves the player highscore and resets it.
   */
  void endGame();
  
  /**
   * \brief Pauses the game.
   */
  void pause();
  
  /**
   * \brief Add points to the #playerScore
   */
  void addPoints(int score);
  
  /**
   * \brief Displays the start menu.
   */
  void menu();
  
  /**
   * \brief Shows the high score table.
   */
  void highScore();
  
  /**
   * \brief Loads the high score from file.
   */
  void loadHighScore();
  
  /**
   * \brief Saves the high score to file.
   */
  void saveHighScore(int score);
 private:
  const int WIDTH; //!< x resolution of the game window
  const int HEIGHT; //!< y resolution of the game window
  SDL_Surface* screen; //!< Screen surface
  std::list<Meteor*> meteorList; //!< List of Meteors
  std::list<EnemyShip*> enemyList; //!< List of enemies
  PlayerShip* player = nullptr; //!< The player
  int playerScore; //!< The player's score
  TTF_Font *pauseFont, *scoreFont, *menuFont;
  SDL_Surface *scoreText, *pauseText, *newGameText, *exitGameText, *highScoreText;
  SDL_Rect *pauseRect, *newGameRect, *exitGameRect, *highScoreRect;
  std::list<int> highscores; //!< High score list
  Uint8* key;
  bool inGame = false, showMenu = true;
  const Uint32 FPS = 1000 / 60;
  Uint32 time;
};
#endif
