#include "EnemyShip.h"

EnemyShip::EnemyShip(int x, int y, int HP, SDL_Surface* img, std::list<SDL_Rect*>& coll)
  : Ship(SPRITE_TYPE::SPRITE_ENEMY, img, x, y, HP, coll), lastShot(0)
{
  
}

EnemyShip::~EnemyShip()
{
  
}

bool EnemyShip::isDead()
{
  return dead && bullets.size() == 0;
}

void EnemyShip::refresh()
{
  if (bullets.size() < 1)
    shoot();
  move();
  
  Ship::refresh();
  if (!dead)
    {
      Sprite::refresh();
    }
}

void EnemyShip::onCollide(Sprite* s)
{
  switch (s->getSpriteType())
    {
    case SPRITE_TYPE::SPRITE_BULLET:
      {
	Bullet* b = dynamic_cast<Bullet*>(s);
	HP -= b->getDMG();
	if (HP < 1)
	  {
	    dead = true;
	    killed = true;
	  }
      }
      break;
    default:
      break;
    }
}


void EnemyShip::shoot()
{
  int bulletSpeed = 5;
  bullets.push_front(new Bullet(pos.x - 1, pos.y + (pos.h / 2), -1, 0, bulletSpeed, 1));
}
