#ifndef METEOR
#define METEOR
#include "Sprite.h"
#include <string>
#include <iostream>

/**
 * \brief A class for representing Meteors.
 */
class Meteor : public Sprite
{
 public:
  /**
   * \brief Constructs Meteor objects
   * 
   * Constructs a Meteor object and initializes it.
   */
  Meteor(int x, int y);
  
  ~Meteor();

  /**
   * \brief Static method for freeing the loaded image
   * 
   * Returns the memory allocated for Meteor images.
   */
  static void freeMeteor();

  /**
   * \brief Updates the Meteor's position.
   * 
   * Updates the Meteor's position according to its speed, and calls Sprite::refresh()
   */
  void refresh();

  /**
   * \brief Collision handler for the Meteor.
   *
   * Defines the collision behavior for Meteors
   * Does nothing. The Meteor is an unstoppable force.
   */
  void onCollide(Sprite* s);
  
 private:
  static SDL_Surface* img; //!< Static image for all Meteors
  static std::list<SDL_Rect*> coll; //!< A list of all Meteors' collision boxes
};

#endif

