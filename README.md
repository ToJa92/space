# SPACE

A shoot 'em up game that was inspired by 'Space Impact' on the Nokia 3310.

# Requirements

* SDL 1.2: https://www.libsdl.org/download-1.2.php
* SDL_image (a version compatible with SDL 1.2): https://www.libsdl.org/projects/SDL_image/
* SDL_ttf (a version compatible with SDL 1.2): https://www.libsdl.org/projects/SDL_ttf/
* GNU GCC, Clang (w/ Apple's LLVM compiler) or some other C++ compiler.
* GNU Make

Your package manager might already have packages available.

For instance, using Homebrew on Apple's OS X (which as of 2014-08-05 installs version 1.2):

```
$ brew install sdl sdl_ttf sdl_image
```

# Running

```
$ make all
$ ./SPACE
```