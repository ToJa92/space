CFLAGS=-Wall -Wextra -pedantic -std=c++11 -g
CC=g++
SDL_FLAGS=`sdl-config --libs --cflags` -lSDL_image -lSDL_ttf

all: SPACE

SPACE: Sprite.o Meteor.o Bullet.o Ship.o Enemy.o PlayerShip.o EnemyShip.o ZigShip.o SinShip.o Game.o main.o
	$(CC) Sprite.o Meteor.o Bullet.o Ship.o Enemy.o PlayerShip.o EnemyShip.o ZigShip.o SinShip.o Game.o main.o -o SPACE $(SDL_FLAGS) 

Game.o: Game.cpp
	$(CC) $(CFLAGS) $(SDL_FLAGS) -c Game.cpp  

main.o: main.cpp
	$(CC) $(CFLAGS) $(SDL_FLAGS) -c main.cpp 

Sprite.o: Sprite.cpp
	$(CC) $(CFLAGS) $(SDL_FLAGS) -c Sprite.cpp 

Meteor.o: Meteor.cpp
	$(CC) $(CFLAGS) $(SDL_FLAGS) -c Meteor.cpp 

Bullet.o: Bullet.cpp
	$(CC) $(CFLAGS) $(SDL_FLAGS) -c Bullet.cpp

Ship.o: Ship.cpp
	$(CC) $(CFLAGS) $(SDL_FLAGS) -c Ship.cpp

Enemy.o: Enemy.cpp
	$(CC) $(CFLAGS) $(SDL_FLAGS) -c Enemy.cpp

PlayerShip.o: PlayerShip.cpp
	$(CC) $(CFLAGS) $(SDL_FLAGS) -c PlayerShip.cpp

EnemyShip.o: EnemyShip.cpp
	$(CC) $(CFLAGS) $(SDL_FLAGS) -c EnemyShip.cpp

ZigShip.o: ZigShip.cpp
	$(CC) $(CFLAGS) $(SDL_FLAGS) -c ZigShip.cpp

SinShip.o: SinShip.cpp
	$(CC) $(CFLAGS) $(SDL_FLAGS) -c SinShip.cpp

clean:
	rm main.o Game.o Sprite.o Meteor.o Bullet.o Ship.o Enemy.o PlayerShip.o EnemyShip.o ZigShip.o SinShip.o SPACE

