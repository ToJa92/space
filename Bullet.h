#ifndef BULLET
#define BULLET
#include "Sprite.h"
#include <string>
#include <list>

/**
 * \brief A class for representing Bullets.
 */
class Bullet : public Sprite
{
 public:
  /**
   * \brief Constructs Bullet objects
   * 
   * Constructs a Bullet object and initializes it.
   */
  Bullet(int x, int y, int xDir, int yDir, int speed, int dmg);

  ~Bullet();

  /**
   * \brief Static method for freeing the loaded image
   * 
   * Returns the memory allocated for Bullet images.
   */
  static void freeBullet();

  /**
   * @return #dmg
   */
  int getDMG();

  /**
   * \brief Updates the Bullet's position.
   * 
   * Updates the Bullet's position according to its speed, and calls Sprite::refresh()
   */
  void refresh();

  /**
   * \brief Collision handler for the Bullet.
   *
   * Defines the collision behavior for Bullets
   * Calls collision handler for the colliding Sprite.
   */
  void onCollide(Sprite* s);
  
 private:
  static SDL_Surface* img; //!< Static image for all Bullets
  static std::list<SDL_Rect*> coll; //!< A list of all Bullets' collision boxes
  int xDir; //!< The x direction for the Bullet
  int yDir; //!< The y direction for the Bullet
  int speed; //!< The speed of the Bullet
  int dmg; //!< The damage of the Bullet
};
#endif
