#ifndef PLAYERSHIP
#define PLAYERSHIP
#include "Ship.h"
#include <iostream>

/**
 * \brief A class for representing a PlayerShip.
 */
class PlayerShip : public Ship
{
 public:
  /**
   * \brief Constructs PlayerShip objects
   * 
   * Constructs a PlayerShip object and initializes it.
   */
  PlayerShip(int x, int y, int HP);
  
  ~PlayerShip();

  /**
   * \brief Static method for freeing the loaded image
   * 
   * Returns the memory allocated for PlayerShip images.
   */
  static void freePlayerShip();  

  /**
   * \brief Collision handler for the PlayerShip.
   * 
   * Defines the collision behavior for PlayerShips
   */
  void onCollide(Sprite* s);

  /**
   * \brief Moves the PlayerShip according to the direction.
   * 
   * @param dir Direction represented as a char ('u' = up, 'd' = down, 'l' = left, 'r' = right).
   */
  void move(char dir);

  /**
   * \brief Shoots a Bullet.
   * 
   * Spawns a Bullet in front of the PlayerShip traveling the direction of the Ship.
   */
  void shoot();
 private:
  Uint32 lastShot; //!< Time of the last shot
  static SDL_Surface* img; //!< Static image for all PlayerShips
  static std::list<SDL_Rect*> coll; //!< A list of all PlayerShips' collision boxes
};
#endif
