#include "Ship.h"

Ship::Ship(SPRITE_TYPE spriteType, SDL_Surface* img, int x, int y, int HP, std::list<SDL_Rect*>& collision)
  : Sprite(spriteType, img, x, y, collision), HP(HP)
{
}

Ship::~Ship()
{
  for (Bullet* b : bullets)
    delete b;
}

int Ship::getHP()
{
  return HP;
}

void Ship::collide(Sprite* s)
{
  for (Bullet* b : bullets)
    {
      b->collide(s);
    }
  Sprite::collide(s);
}

void Ship::refresh()
{
  //Removes all dead bullets. Calls refresh for all non-dead bullets.
  std::list<Bullet*>::iterator b = bullets.begin();
  while (b != bullets.end())
    {
      if ((*b)->isDead())
	{
	  delete *b;
	  b = bullets.erase(b);
	} 
      else
	{
	  (*b)->refresh();
	  ++b;
	}
    }
}


void Ship::draw()
{
  for (Bullet* b : bullets) {
    b->draw();
  }
  if (!dead)
    Sprite::draw();
}
