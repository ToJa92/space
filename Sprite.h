#ifndef SPRITE
#define SPRITE
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <list>
#include <iostream>

/**
 * \brief An abstract class handling sprites.
 * 
 * Base class for every game object.
 */
class Sprite
{
 public:
  enum class SPRITE_TYPE {
    SPRITE_METEOR,
    SPRITE_ENEMY,
    SPRITE_PLAYER,
    SPRITE_BULLET
  };
  /**
   * \brief Constructs Sprite objects
   * 
   * Constructs a Sprite object and initializes it.
   */
  Sprite(SPRITE_TYPE spriteType, SDL_Surface* img, int x, int y, std::list<SDL_Rect*>& collision);
  
  virtual ~Sprite();
  
  /**
   * \brief Returns the Sprite's image.
   * 
   * @return A pointer to an SDL_Surface containing the Sprite's image.
   */
  virtual SDL_Surface* getImg();
  
  /**
   * \brief Refreshes the Sprite.
   * 
   * Sets #dead equal to true if the Sprite leaves the game area.
   */
  virtual void refresh();
  
  /**
   * \brief Returns the value of #dead
   * 
   * @return true if dead, otherwise false.
   */
  virtual bool isDead();
  
  /**
   * \brief Checks if the Sprite collides with another Sprite.
   * 
   * Calls onCollide() if any collision boxes overlap.
   */
  virtual void collide(Sprite* s);
  
  /**
   * \brief Abstract method for collision handling.
   * 
   * @see collide()
   */
  virtual void onCollide(Sprite* s) = 0;
  
  /**
   * \brief Returns a pointer to #pos
   * 
   * @return A pointer to an SDL_Rect containing the position and size.
   */
  SDL_Rect* getPos();
  
  /**
   * @return The width of the Sprite.
   */
  virtual int getW();
  
  /**
   * @return The height of the Sprite.
   */
  virtual int getH();
  
  /**
   * @return The x coordinate of the Sprite.
   */
  virtual int getX();
  
  /**
   * @return The y coordinate of the Sprite.
   */
  virtual int getY();
  
  /**
   * \brief Draws the Sprite.
   * 
   * Blits the Sprite's image to the screen.
   */
  virtual void draw();
  
  /**
   * \brief Returns #spriteType
   * 
   * @return #spriteType
   */
  SPRITE_TYPE getSpriteType() { return spriteType; };

  static SDL_Surface* loadImg(std::string const& file);
 protected:
  SPRITE_TYPE spriteType; //!< The Sprite type
  SDL_Surface* img; //!< A pointer to the Sprite's image
  std::list<SDL_Rect*> collision; //!< A list of the Sprite's collision boxes.
  SDL_Rect pos; //!< Contains the Sprite's position and size
  bool dead; //!< Indicates whether the Sprite is dead or not
};
#endif
