#ifndef ENEMY
#define ENEMY
#include <iostream>

/**
 * \brief An abstract class for representing an Enemy.
 */
class Enemy
{
 public:
  /**
   * \brief Constructs Enemy objects
   * 
   * Constructs an Enemy object and initializes it.
   */
  Enemy();
  /**
   * @return #killed
   */
  bool isKilled();
  /**
   * \brief Abstract method for retrieving points
   */
  virtual int getPoints() = 0;
 protected:
  bool killed; //!< True if killed by a PlayerShip, false otherwise
};
#endif
