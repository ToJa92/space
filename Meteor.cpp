#include "Meteor.h"

Meteor::Meteor(int x, int y)
  : Sprite(SPRITE_TYPE::SPRITE_METEOR, img, x, y, coll)
{
}

Meteor::~Meteor()
{
}

void Meteor::freeMeteor()
{
  SDL_FreeSurface(Meteor::img);
  for(auto x : Meteor::coll)
    delete x;
}

void Meteor::refresh()
{
  pos.x -= 2;
  Sprite::refresh();
}

void Meteor::onCollide(Sprite*)
{
  
}

SDL_Surface* Meteor::img = loadImg("comet.png");
std::list<SDL_Rect*> Meteor::coll = { new SDL_Rect {0,0,47,47} };
