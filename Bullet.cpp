#include "Bullet.h"

Bullet::Bullet(int x, int y, int xDir, int yDir, int speed, int dmg)
  : Sprite(SPRITE_TYPE::SPRITE_BULLET, img, x, y, coll), xDir(xDir), yDir(yDir), speed(speed), dmg(dmg)
{
}

Bullet::~Bullet() {}

void Bullet::freeBullet()
{
  SDL_FreeSurface(Bullet::img);
  for(auto x : Bullet::coll)
    delete x;
}

int Bullet::getDMG()
{
  return dmg;
}

void Bullet::onCollide(Sprite* s)
{
  s->onCollide(this);
  switch(s->getSpriteType())
    {
    case SPRITE_TYPE::SPRITE_PLAYER:
    case SPRITE_TYPE::SPRITE_ENEMY:
      dead = true;
      break;
    case SPRITE_TYPE::SPRITE_METEOR:
      dead = true;
      break;
    case SPRITE_TYPE::SPRITE_BULLET:
    default:
      break;


    }

}

void Bullet::refresh()
{
  pos.x += xDir*speed;
  pos.y += yDir*speed;
  Sprite::refresh();
}

SDL_Surface* Bullet::img = loadImg("bullet.png");
std::list<SDL_Rect*> Bullet::coll = { new SDL_Rect {0,0,3,3} };
