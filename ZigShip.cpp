#include "ZigShip.h"

ZigShip::ZigShip(int x, int y, int HP)
  : EnemyShip(x, y, HP, img, coll), startX(x), doubleY(y)
{
}

void ZigShip::move()
{
  pos.x -= 1;
  pos.y += ((startX - pos.x / 100) % 2 == 0) ? 1 : -1;
}

int ZigShip::getPoints()
{
  return points;
}

void ZigShip::freeEnemyShip()
{
  SDL_FreeSurface(ZigShip::img);
  for(auto x : ZigShip::coll)
    delete x;
}

void ZigShip::shoot()
{
  int bulletSpeed = 5;
  bullets.push_front(new Bullet(pos.x - 1, pos.y + 11, -1, 0, bulletSpeed, 1));
  bullets.push_front(new Bullet(pos.x - 1, pos.y + 44, -1, 0, bulletSpeed, 1));
}

int ZigShip::points = 10;
SDL_Surface* ZigShip::img = loadImg("Zig.png");
std::list<SDL_Rect*> ZigShip::coll = { new SDL_Rect {0,0,64,58} };
