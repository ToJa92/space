#include "SinShip.h"

SinShip::SinShip(int x, int y, int HP)
  : EnemyShip(x, y, HP, img, coll), startY(y), doubleY(y)
{
}

void SinShip::move()
{
  pos.x -= 1;
  doubleY += sin((pos.x + startY) * 3.14152 / 180) * 2;
  pos.y = doubleY;
  //std::cout << doubleY << std::endl << "\t"  << pos.y << std::endl;
}

int SinShip::getPoints()
{
  return points;
}

void SinShip::freeEnemyShip()
{
  SDL_FreeSurface(SinShip::img);
  for(auto x : SinShip::coll)
    delete x;
}

int SinShip::points = 5;
SDL_Surface* SinShip::img = loadImg("enemy.png");
std::list<SDL_Rect*> SinShip::coll = { new SDL_Rect {0,0,90,63} };
