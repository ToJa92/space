#ifndef ZIGSHIP
#define ZIGSHIP
#include "EnemyShip.h"
#include <cmath>

/**
 * \brief A class for representing a ZigShip.
 */
class ZigShip : public EnemyShip
{
public:
  /**
   * \brief Constructs ZigShip objects
   * 
   * Constructs a ZigShip object and initializes it.
   */
  ZigShip(int x, int y, int HP);
  /**
   * \brief Moves the ZigShip according to a pattern
   */
  void move();
  /**
   * \brief Returns the number of #points awarded when killed
   */
  int getPoints();
  /**
   * \brief Static method for freeing the loaded image
   * 
   * Returns the memory allocated for ZigShip images.
   */
  static void freeEnemyShip();

  void shoot();
private:
  int startX; //!< Start x coordinate
  double doubleY; //!< Y coordinate as a floating point number
  static int points; //!< Number of points
  static SDL_Surface* img; //!< Static image for all ZigShips
  static std::list<SDL_Rect*> coll;//!< A list of all ZigShips' collision boxes
};
#endif
