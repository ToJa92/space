#include "PlayerShip.h"

PlayerShip::PlayerShip(int x, int y, int HP)
  : Ship(SPRITE_TYPE::SPRITE_PLAYER, img, x, y, HP, coll), lastShot(0)
{
  
}

PlayerShip::~PlayerShip() {}

void PlayerShip::freePlayerShip()
{
  SDL_FreeSurface(PlayerShip::img);
  for(auto x : PlayerShip::coll)
    delete x;
}

void PlayerShip::onCollide(Sprite* s)
{
  switch(s->getSpriteType())
    { 
    case SPRITE_TYPE::SPRITE_METEOR:
      dead = true;
      break;
    case SPRITE_TYPE::SPRITE_ENEMY:
      dead = true;
      break;
    case SPRITE_TYPE::SPRITE_BULLET:
      {
	Bullet* b = dynamic_cast<Bullet*>(s);
	HP -= b->getDMG();
	if (HP < 1) 
	  dead = true;
      }
      break;
    default:
      break;
    }
}

void PlayerShip::move(char dir)
{
  SDL_Rect const& frame = SDL_GetVideoSurface()->clip_rect;
  int speed = 5;
  switch(dir)
    {
    case 'u':
      pos.y = (pos.y - speed < frame.y)? frame.y : pos.y - speed;
      break;
    case 'd':
      pos.y = (pos.y + pos.h + speed > frame.h + frame.y)? frame.h - pos.h + frame.y : pos.y + speed;
      break;
    case 'l':
      pos.x = (pos.x - speed < frame.x)? frame.x : pos.x - speed;
      break;
    case 'r':
      pos.x = (pos.x + pos.w + speed > frame.w + frame.x)? frame.w - pos.w + frame.x : pos.x + speed;
      break;
    default:
      break;
    }
}

void PlayerShip::shoot()
{
  int bulletSpeed = 5;
  if(lastShot < SDL_GetTicks() - 300)
    {
      //Ship::shoot();
      bullets.push_back(new Bullet(pos.x + pos.w + 1, pos.y + (pos.h / 2), 1, 0, bulletSpeed, 1));
      lastShot = SDL_GetTicks();
    }
}

SDL_Surface* PlayerShip::img = loadImg("spaceship.png");
std::list<SDL_Rect*> PlayerShip::coll = { new SDL_Rect {0,0,63,63} };
